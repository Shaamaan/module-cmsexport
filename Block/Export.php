<?php

namespace Shaamaan\CmsExport\Block;


use Magento\Framework\View\Element\Template;

class Export extends \Magento\Framework\View\Element\Template
{

    private $defaultLogic;
    private $url;

    /**
     * MyDefault constructor.
     */
    public function __construct(Template\Context $context, array $data = [],
                                \Shaamaan\CmsExport\Model\ExportLogic $defaultLogic,
                                \Magento\Framework\UrlInterface $url)
    {
        parent::__construct($context, $data);
        $this->defaultLogic = $defaultLogic;
        $this->url = $url;
    }

    public function getAddUrl() : string
    {
        //* - zwraca ten element, z którego to jest wywoływane
        return $this->url->getUrl('*/*/export');
        //powyzsze zwroci lesson1(lub ex1)/index/add
    }
}