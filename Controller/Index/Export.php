<?php

namespace Shaamaan\Lesson1\Controller\Index;

use Magento\Framework\App\Action\Context;
use Magento\Framework\Controller\ResultFactory;
use Magento\Framework\Exception\LocalizedException;
//use Shaamaan\Lesson1\Api\Data\LessonDataInterface;
//use Shaamaan\Lesson1\Api\LessonDataRepositoryInterface;
//use Shaamaan\Lesson1\Model\ResourceModel\LessonData;

class Export extends \Magento\Framework\App\Action\Action
{
    public function __construct
    (
        Context $context
//        LessonDataRepositoryInterface $lessonDataRepository,
//        \Shaamaan\Lesson1\Model\ResourceModel\LessonData $lessonDataResourceModel,
//        \Shaamaan\Lesson1\Api\Data\LessonDataInterfaceFactory $lessonDataFactory
    )
    {
        parent::__construct($context);
//        $this->lessonDataRepo = $lessonDataRepository;
//        $this->lessonDataResourceModel = $lessonDataResourceModel;
//        $this->lessonDataFactory = $lessonDataFactory;
    }


    public function execute()
    {
        $allParams = $this->getRequest()->getParams();
        $dataParam = $this->getRequest()->getParam('lesson1_data');
        if ($allParams['lesson1_data'] !== $dataParam) {
            throw new LocalizedException(__('This... is wroooong!')); // cos this should never happen
        }

        $repoTypeParam = '';
        if (array_key_exists('submitType', $allParams)) {
            $repoTypeParam = $allParams['submitType'];
        }
        if ($repoTypeParam === '') {
            throw new LocalizedException(__('Select a submit type!'));
        }
        if ($dataParam === '') {
            throw new LocalizedException(__('Submit some data!'));
        }

        if ($repoTypeParam === 'repo') {
            $finalData = $this->saveByRepo($dataParam);
        } else {
            $finalData = $this->saveByResource($dataParam);
        }

        /** @var \Magento\Framework\Controller\Result\Redirect $resultRedirect */
        $resultRedirect = $this->resultFactory->create(ResultFactory::TYPE_REDIRECT);
//        $resultRedirect->setUrl($this->_url->getUrl('*/*')); //'index' jest z automatu, dlatego ostatni fragment można pominąć (see MyForm.php)
//        $this->messageManager->addSuccessMessage(__('THIS IS A DEBUG STRING \'%1\'', $finalData));
        return $resultRedirect;
    }

//    private function saveByRepo(string $data): string
//    {
//        /** @var \Shaamaan\Lesson1\Api\Data\LessonDataInterface $model */
//        $model = $this->lessonDataFactory->create();
//        $model->setLessonData('added via REPO: ' . $data);
//        $this->lessonDataRepo->save($model);
//        return $model->getLessonData();
//    }
//
//    private function saveByResource(string $data): string
//    {
//        /** @var \Shaamaan\Lesson1\Api\Data\LessonDataInterface $model */
//        $model = $this->lessonDataFactory->create();
//        $model->setLessonData('added via RESOURCE: ' . $data);
//        $this->lessonDataResourceModel->save($model);
//        return $model->getLessonData();
//    }

}